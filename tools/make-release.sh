#!/bin/sh
# don't quote $appimage_zsync_arg
#appimage_zsync_arg=
#appimage_zsync_url=
# set to automatically set $appimage_zsync_arg and $appimage_zsync_url
#appimage_zsync_url_dir=
#appimagetool_file=
#appimagetool_url=
#arch=
cdir=$PWD
#clean=
#epoch=
#gdir=
#gitdir=
#git_remote_url=
#git_update=
#made_source_dir=
#odir=
#project=
#skip_appimage=
#skip_srcpkg=
#skip_tar=
#skip_zip=
#tar_lzip=
#tar_zstd=
#update_appimagetool=
#version=
echo

[ -z "$project" ] && project=srcpkg2git
[ -z "$version" ] && version=0.1

for args; do
  case $args in
    -c|--clean|clean)
      clean=1
      ;;
    -h|--help|help)
      echo "  usage: $0 [version] [-c] [-h] [-u]"
      echo "   e.g.: $0 ${version:-0.1} -c"
      echo
      echo "positional arguments:"
      echo "  version          version number (e.g., ${version:-0.1})"
      echo
      echo "options:"
      #echo "  -c, --clean      clean/remove (temporary) source dir (${project:=srcpkg2git}/) after make"
      echo "  -c, --clean      clean/remove (temporary) source dir (${project:-srcpkg2git-0.1}/) after make"
      echo "  -h, --help       show this help message and exit"
      echo "  -u, --update     sync/update git repository (git pull) before make"
      echo
      echo "  --skip-appimage  skip making AppImage releases"
      echo "  --skip-srcpkg    skip making srcpkg releases"
      echo "  --skip-tar       skip making tarball (tar.*) releases"
      echo "  --skip-zip       skip making zip release"
      echo
      echo "  --tar-lzip       use 'tar' binary instead of 'lzip' binary to make tar.lzip"
      echo "  --tar-zstd       use 'tar' binary instead of 'zstd' binary to make tar.zst"
      echo "  --update-ait     update (remove/replace/redownload) appimagetool.AppImage"
      exit 0
      ;;
    -u|--update|update|--git-update|git-update)
      git_update=1
      ;;
    --skip-appimage)
      skip_appimage=1
      ;;
    --skip-srcpkg)
      skip_srcpkg=1
      ;;
    --skip-tar)
      skip_tar=1
      ;;
    --skip-zip)
      skip_zip=1
      ;;
    --tar-lzip)
      tar_lzip=1
      ;;
    --tar-zstd)
      tar_zstd=1
      ;;
    --update-ait|--update-appimagetool)
      update_appimagetool=1
      ;;
    *)
      echo "version: $args"
      version=$args
      #echo "version: $version"
      echo
      ;;
  esac
done

# 2024-04-01 12:00:00 -0700
#[ -z "$epoch" ] && epoch=1711998000
[ -z "$epoch" ] && epoch=0
[ -z "$git_remote_url" ] && git_remote_url=https://gitlab.com/evlaV/srcpkg2git.git
#[ -z "$version" ] && version=-0.1 || version=-$version
[ -n "$version" ] && version=-$version

srcpkg_tgz_file=${project:=srcpkg2git}$version.src.tar.gz
srcpkg_txz_file=${project:=srcpkg2git}$version.src.tar.xz

tar_file=${project:=srcpkg2git}$version.tar
#tbz2_file=${project:=srcpkg2git}$version.tar.bz2
tbz2_file=$tar_file.bz2
#tgz_file=${project:=srcpkg2git}$version.tar.gz
tgz_file=$tar_file.gz
#tlz_file=${project:=srcpkg2git}$version.tar.lz
tlz_file=$tar_file.lz
#tlzma_file=${project:=srcpkg2git}$version.tar.lzma
tlzma_file=$tar_file.lzma
#txz_file=${project:=srcpkg2git}$version.tar.xz
txz_file=$tar_file.xz
#tzst_file=${project:=srcpkg2git}$version.tar.zst
tzst_file=$tar_file.zst

zip_file=${project:=srcpkg2git}$version.zip

make_source_dir() {
# remove source dir (clean)
rm -fr "${project:=srcpkg2git}$version"/
# make source dir
mkdir -p "${project:=srcpkg2git}$version"
# make source dir and srcpkg dir
#mkdir -p "${project:=srcpkg2git}$version/${project:=srcpkg2git}"
#mkdir -p "${project:=srcpkg2git}$version/${project:=srcpkg2git}/${project:=srcpkg2git}"
if [ -z "$skip_srcpkg" ] && [ -d ".git" ]; then
  # make srcpkg dir - requires remove srcpkg dir (clean) below
  cp -afr .git/ "${project:=srcpkg2git}$version/${project:=srcpkg2git}"
  #cp -afr .git/ "${project:=srcpkg2git}$version/${project:=srcpkg2git}/${project:=srcpkg2git}"
  # use existing srcpkg dir - requires make source dir and srcpkg dir above
  #cp -afr .git/* "${project:=srcpkg2git}$version/${project:=srcpkg2git}"/
  #cp -afr .git/* "${project:=srcpkg2git}$version/${project:=srcpkg2git}/${project:=srcpkg2git}"/
fi
# AppImage (symlink -> file)
cp -afLr appimage/ "${project:=srcpkg2git}$version"/ || exit 1
# bash
#cp -afr alpm/ config/ Containerfile cron/ Dockerfile git-commit.sh git-credential-bashelper.sh git-credential-shelper.sh git-remote.sh images/ lib/ LICENSE LICENSE.GPL LICENSE.MPL Makefile make-release.sh README.html README.md {srcpkg2git,srcpkg-dl,srcpkg-dl-bot}-{local,remote,remote-tag}.Containerfile srcpkg2git.mk srcpkg2git.sh srcpkg-dl-bot.mk srcpkg-dl.mk srcpkg-dl.sh systemd/ tools/ xdg/ "${project:=srcpkg2git}$version"/ || exit 2
# sh
cp -afr alpm/ config/ Containerfile cron/ Dockerfile git-commit.sh git-credential-bashelper.sh git-credential-shelper.sh git-remote.sh images/ lib/ LICENSE LICENSE.GPL LICENSE.MPL Makefile make-release.sh README.html README.md srcpkg2git-local.Containerfile srcpkg2git.mk srcpkg2git-remote.Containerfile srcpkg2git-remote-tag.Containerfile srcpkg2git.sh srcpkg-dl-bot-local.Containerfile srcpkg-dl-bot.mk srcpkg-dl-bot-remote.Containerfile srcpkg-dl-bot-remote-tag.Containerfile srcpkg-dl-local.Containerfile srcpkg-dl.mk srcpkg-dl-remote.Containerfile srcpkg-dl-remote-tag.Containerfile srcpkg-dl.sh systemd/ tools/ xdg/ "${project:=srcpkg2git}$version"/ || exit 2
# timestamp source dir
if hash touch 2>/dev/null; then
  # bash 4.0+
  #for file in "${project:=srcpkg2git}$version"/**/*; do
  # sh
  # '"${project:=srcpkg2git}$version"/*/*/*/*/*/*' (and maybe '"${project:=srcpkg2git}$version"/*/*/*/*/*/*/*') used for srcpkg dir (e.g., $project/logs/refs/remotes/origin/HEAD)
  for file in "${project:=srcpkg2git}$version"/*/*/*/*/*/* "${project:=srcpkg2git}$version"/*/*/*/*/* "${project:=srcpkg2git}$version"/*/*/*/* "${project:=srcpkg2git}$version"/*/*/* "${project:=srcpkg2git}$version"/*/*/.* "${project:=srcpkg2git}$version"/*/* "${project:=srcpkg2git}$version"/* "${project:=srcpkg2git}$version"; do
    if [ -e "$file" ]; then
      #touch -d @0 -h "$file"
      touch -d @"${epoch:=0}" -h "$file"
    fi
  done
fi
made_source_dir=1
}

if hash git 2>/dev/null; then
  gitdir=$(git rev-parse --show-toplevel 2>/dev/null)
fi
#if [ -d "$gitdir" ] && [ "$gitdir" != "$cdir" ]; then
if [ -d "$gitdir" ] && [ "$gitdir" != "$PWD" ]; then
  #echo "changed directory ($PWD -> $gitdir)"
  echo "changed directory: $PWD -> $gitdir"
  cd "$gitdir"
  #echo "changed directory ($PWD)"
  #echo "changed directory: $PWD"
  #echo "changed directory ($cdir -> $PWD)"
  #echo "changed directory: $cdir -> $PWD"
  echo
  odir=$cdir
  #cdir=$gitdir
  cdir=$PWD
# tools/ -> .. or dir/ -> ..
#elif [ "${PWD##*/}" = "tools" ] || [ ! -e "srcpkg2git.sh" ] && [ -e "../srcpkg2git.sh" ]; then
elif [ "${PWD##*/}" = "tools" ] || [ ! -f "srcpkg2git.sh" ] && [ -f "../srcpkg2git.sh" ]; then
  cd ..
  #echo "changed directory ($PWD)"
  #echo "changed directory: $PWD"
  #echo "changed directory ($cdir -> $PWD)"
  echo "changed directory: $cdir -> $PWD"
  echo
  odir=$cdir
  cdir=$PWD
fi

# git update
if [ -n "$git_update" ]; then
  if hash git 2>/dev/null; then
    if [ -d ".git" ]; then
      git pull --rebase || exit 3
      # git clean (git gc)
      #git gc
      #git gc --prune=now
      git gc --aggressive --prune=now
    #else
    elif [ ! -d "${project:=srcpkg2git}" ]; then
      #rm -fr "${project:=srcpkg2git}"/
      git clone "${git_remote_url:=https://gitlab.com/evlaV/srcpkg2git.git}" "${project:=srcpkg2git}" && cd "${project:=srcpkg2git}" || exit 4
      #echo "changed directory ($PWD)"
      #echo "changed directory: $PWD"
      #echo "changed directory ($cdir -> $PWD)"
      echo "changed directory: $cdir -> $PWD"
      echo
      #odir=$cdir
      #cdir=$PWD
      gdir=$PWD
    else
      exit 4
    fi
  else
    echo "error: git not found! install git to sync/update source"
    echo
    exit 5
  fi
fi

# template -> config (.conf.template -> .conf)
#[ ! -s "git-remote.conf" ] && [ -s "config/git-remote.conf.template" ] && cp -af config/git-remote.conf.template git-remote.conf
#[ ! -s "srcpkg-dl.conf" ] && [ -s "config/srcpkg-dl.conf.template" ] && cp -af config/srcpkg-dl.conf.template srcpkg-dl.conf

if [ -z "$made_source_dir" ]; then
  make_source_dir
fi

# make tarball (tar.*) releases
if hash tar 2>/dev/null; then
  if [ -z "$skip_srcpkg" ]; then
    # make srcpkg releases
    if [ -d "${project:=srcpkg2git}$version/${project:=srcpkg2git}" ]; then
      # make tar.gz srcpkg release
      #echo "making srcpkg: $srcpkg_tgz_file"
      echo "making srcpkg ($srcpkg_tgz_file)"
      echo
      GZIP=-9 tar -cpvzf "${srcpkg_tgz_file:?}" "${project:=srcpkg2git}$version/${project:=srcpkg2git}"/
      echo
      # make tar.xz srcpkg release
      #echo "making srcpkg: $srcpkg_txz_file"
      echo "making srcpkg ($srcpkg_txz_file)"
      echo
      XZ_OPT=-9 tar -cJpvf "${srcpkg_txz_file:?}" "${project:=srcpkg2git}$version/${project:=srcpkg2git}"/
      echo
      # remove srcpkg dir (clean)
      rm -fr "${project:=srcpkg2git}$version/${project:=srcpkg2git}"/
    fi
  else
    echo "skipped making srcpkg releases"
    echo
  fi
  if [ -z "$skip_tar" ]; then
    # make tar release
    echo "making $tar_file"
    echo
    tar -cpvf "${tar_file:?}" "${project:=srcpkg2git}$version"/
    echo
    # make tar.bz2 release
    echo "making $tbz2_file"
    echo
    BZIP2=-9 tar -cjpvf "${tbz2_file:?}" "${project:=srcpkg2git}$version"/
    echo
    # make tar.gz release
    echo "making $tgz_file"
    echo
    GZIP=-9 tar -cpvzf "${tgz_file:?}" "${project:=srcpkg2git}$version"/
    echo
    # make tar.lz release
    echo "making $tlz_file"
    echo
    if hash lzip 2>/dev/null && [ -z "$tar_lzip" ]; then
      tar -I 'lzip -9' -cpvf "${tlz_file:?}" "${project:=srcpkg2git}$version"/
    else
      if [ -z "$tar_lzip" ]; then
        echo "warning: lzip not found! using 'tar --lzip' instead of 'lzip'"
        echo
      fi
      tar --lzip -cpvf "${tlz_file:?}" "${project:=srcpkg2git}$version"/
    fi
    echo
    # make tar.lzma release
    echo "making $tlzma_file"
    echo
    #tar -I 'xz --format=lzma -9' -cpvf "${tlzma_file:?}" "${project:=srcpkg2git}$version"/
    # 'xz --format=lzma' alias
    #tar -I 'lzma -9' -cpvf "${tlzma_file:?}" "${project:=srcpkg2git}$version"/
    XZ_OPT=-9 tar --lzma -cpvf "${tlzma_file:?}" "${project:=srcpkg2git}$version"/
    echo
    # make tar.xz release
    echo "making $txz_file"
    echo
    XZ_OPT=-9 tar -cJpvf "${txz_file:?}" "${project:=srcpkg2git}$version"/
    echo
    # make tar.zst release
    echo "making $tzst_file"
    echo
    if hash zstd 2>/dev/null && [ -z "$tar_zstd" ]; then
      tar -I 'zstd -19' -cpvf "${tzst_file:?}" "${project:=srcpkg2git}$version"/
    else
      if [ -z "$tar_zstd" ]; then
        echo "warning: zstd not found! using 'tar --zstd' instead of 'zstd'"
        echo
      fi
      ZSTD_CLEVEL=19 tar --zstd -cpvf "${tzst_file:?}" "${project:=srcpkg2git}$version"/
    fi
    echo
  else
    echo "skipped making tarball (tar.*) releases"
    echo
  fi
else
  #echo "error: tar not found! install tar to make:"
  echo "warning: tar not found! install tar to make:"
  echo "$srcpkg_tgz_file"
  echo "$srcpkg_txz_file"
  echo
  echo "$tar_file"
  echo "$tbz2_file"
  echo "$tgz_file"
  echo "$tlz_file"
  echo "$tlzma_file"
  echo "$txz_file"
  echo "$tzst_file"
  echo
  #exit 6
fi

if [ -z "$skip_zip" ]; then
  # make zip release
  if hash zip 2>/dev/null; then
    echo "making $zip_file"
    echo
    zip -r "${zip_file:?}" "${project:=srcpkg2git}$version"/
    echo
  else
    #echo "error: zip not found! install zip to make:"
    echo "warning: zip not found! install zip to make:"
    echo "$zip_file"
    echo
    #exit 7
  fi
else
  echo "skipped making zip release"
  echo
fi

# timestamp releases
if hash touch 2>/dev/null; then
  for file in $srcpkg_tgz_file $srcpkg_txz_file $tar_file $tbz2_file $tgz_file $tlz_file $tlzma_file $txz_file $tzst_file $zip_file; do
    #if [ -e "$file" ]; then
    if [ -f "$file" ]; then
      #touch -d @0 "$file"
      touch -d @"${epoch:=0}" "$file"
    fi
  done
fi

if [ -z "$skip_appimage" ]; then
  # make AppImage releases
  # set to automatically set $appimage_zsync_arg and $appimage_zsync_url
  [ -z "$appimage_zsync_url_dir" ] && appimage_zsync_url_dir=
  # detect architecture
  if [ -z "$arch" ]; then
    if hash uname 2>/dev/null; then
      arch=$(uname -m)
    else
      #arch=aarch64
      arch=x86_64
      #echo "assuming (ASS-U-ME) architecture ($arch)"
      echo "assuming (ASS-U-ME) architecture: $arch"
      echo
    fi
  fi
  # download appimagetool
  [ -z "$appimagetool_file" ] && appimagetool_file=appimagetool-$arch.AppImage
  [ -z "$appimagetool_url" ] && appimagetool_url=https://github.com/AppImage/appimagetool/releases/download/continuous/$appimagetool_file
  [ -n "$update_appimagetool" ] && rm -f "${appimagetool_file:=appimagetool-x86_64.AppImage}"
  #if [ ! -s "$appimagetool_file" ]; then
  if [ ! -s "${appimagetool_file:=appimagetool-x86_64.AppImage}" ]; then
    #echo "downloading ($arch) appimagetool (${appimagetool_url:=https://github.com/AppImage/appimagetool/releases/download/continuous/$appimagetool_file})"
    echo "downloading ($arch) appimagetool: ${appimagetool_url:=https://github.com/AppImage/appimagetool/releases/download/continuous/$appimagetool_file}"
    echo
    if hash curl 2>/dev/null; then
      curl -Lo "$appimagetool_file" "${appimagetool_url:=https://github.com/AppImage/appimagetool/releases/download/continuous/$appimagetool_file}" || exit 8
    elif hash wget 2>/dev/null; then
      wget -O "$appimagetool_file" "${appimagetool_url:=https://github.com/AppImage/appimagetool/releases/download/continuous/$appimagetool_file}" || exit 9
    else
      echo "error: curl or wget not found! install curl or wget to download appimagetool"
      echo
      echo "or:"
      echo "1. manually download: ${appimagetool_url:=https://github.com/AppImage/appimagetool/releases/download/continuous/$appimagetool_file}"
      echo "2. save as: $PWD/$appimagetool_file"
      echo "3. set the executable mode bit: chmod +x \"$appimagetool_file\""
      echo
      exit 10
    fi
    echo
    #if [ ! -x "$appimagetool_file" ]; then
    if [ ! -x "${appimagetool_file:=appimagetool-x86_64.AppImage}" ]; then
      if ! hash chmod 2>/dev/null || ! chmod +x "$appimagetool_file"; then
        echo "error: $appimagetool_file executable mode bit not set!"
        echo
        echo "set the executable mode bit: chmod +x \"$appimagetool_file\""
        echo
        exit 11
      fi
    fi
  fi
  #if [ -s "$appimagetool_file" ]; then
  if [ -s "${appimagetool_file:=appimagetool-x86_64.AppImage}" ]; then
    export ARCH
    for ARCH in aarch64 armhf i686 x86_64; do
      for VAR in srcpkg2git srcpkg-dl srcpkg-dl-bot; do
        [ -n "$appimage_zsync_url_dir" ] && appimage_zsync_url=$appimage_zsync_url_dir/$VAR$version-$ARCH.AppImage.zsync || appimage_zsync_url=
        [ -n "$appimage_zsync_url" ] && appimage_zsync_arg="-u zsync|$appimage_zsync_url" || appimage_zsync_arg=
        #echo "making $VAR AppImage: $VAR$version-$ARCH.AppImage"
        echo "making $VAR AppImage ($VAR$version-$ARCH.AppImage)"
        echo
        #if [ -d "$project$version/appimage/$VAR" ]; then
        if [ -d "${project:=srcpkg2git}$version/appimage/$VAR" ]; then
          # $project$version/appimage/$VAR/
          # make AppImage release ($VAR)
          #ARCH=$ARCH ./"$appimagetool_file" "${project:=srcpkg2git}$version"/appimage/"$VAR" $appimage_zsync_arg
          #ARCH=$ARCH ./"$appimagetool_file" "${project:=srcpkg2git}$version/appimage/$VAR" $appimage_zsync_arg
          #ARCH=$ARCH ./"$appimagetool_file" "${project:=srcpkg2git}$version"/appimage/"$VAR" "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
          #ARCH=$ARCH ./"$appimagetool_file" "${project:=srcpkg2git}$version/appimage/$VAR" "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
          # requires export ARCH above
          #./"$appimagetool_file" "${project:=srcpkg2git}$version"/appimage/"$VAR" $appimage_zsync_arg
          #./"$appimagetool_file" "${project:=srcpkg2git}$version/appimage/$VAR" $appimage_zsync_arg
          #./"$appimagetool_file" "${project:=srcpkg2git}$version"/appimage/"$VAR" "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
          ./"$appimagetool_file" "${project:=srcpkg2git}$version/appimage/$VAR" "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
        elif [ -d "appimage/$VAR" ]; then
          # symlink -> file ($VAR/ -> $VAR.AppDir/)
          # remove AppImage AppDir (clean)
          #rm -fr appimage/*.AppDir/
          rm -fr appimage/"$VAR".AppDir/
          # make AppImage AppDir ($VAR)
          #echo "making $VAR AppImage AppDir: appimage/$VAR.AppDir/"
          echo "making $VAR AppImage AppDir (appimage/$VAR.AppDir/)"
          echo
          #mkdir -p appimage/"$VAR".AppDir
          #cp -afLr appimage/"$VAR"/ appimage/"$VAR".AppDir/ || exit 12
          cp -afLr appimage/"$VAR"/ appimage/"$VAR".AppDir/ || continue
          if hash touch 2>/dev/null; then
            # bash 4.0+
            #for file in appimage/"$VAR"/**/*; do
            #for file in appimage/"$VAR".AppDir/**/*; do
            # sh
            #for file in appimage/"$VAR"/*/*/*/* appimage/"$VAR"/*/*/* appimage/"$VAR"/*/* appimage/"$VAR"/* appimage/"$VAR"; do
            for file in appimage/"$VAR".AppDir/*/*/*/* appimage/"$VAR".AppDir/*/*/* appimage/"$VAR".AppDir/*/* appimage/"$VAR".AppDir/*/.* appimage/"$VAR".AppDir/* appimage/"$VAR".AppDir; do
              if [ -e "$file" ]; then
                #touch -d @0 -h "$file"
                touch -d @"${epoch:=0}" -h "$file"
              fi
            done
          fi
          # make AppImage release ($VAR)
          #ARCH=$ARCH ./"$appimagetool_file" appimage/"$VAR" $appimage_zsync_arg
          #ARCH=$ARCH ./"$appimagetool_file" appimage/"$VAR" "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
          # requires export ARCH above
          #./"$appimagetool_file" appimage/"$VAR" $appimage_zsync_arg
          #./"$appimagetool_file" appimage/"$VAR" "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
          # make AppImage release ($VAR.AppDir)
          #ARCH=$ARCH ./"$appimagetool_file" appimage/"$VAR".AppDir $appimage_zsync_arg
          #ARCH=$ARCH ./"$appimagetool_file" appimage/"$VAR".AppDir "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
          # requires export ARCH above
          #./"$appimagetool_file" appimage/"$VAR".AppDir $appimage_zsync_arg
          ./"$appimagetool_file" appimage/"$VAR".AppDir "$VAR$version-$ARCH".AppImage $appimage_zsync_arg
        fi
        echo
      done
    done
  fi
else
  echo "skipped making AppImage releases"
  echo
fi

# timestamp AppImage releases
if hash touch 2>/dev/null; then
  for file in *.AppImage *.zsync; do
    #if [ -e "$file" ]; then
    if [ -f "$file" ]; then
      #touch -d @0 "$file"
      touch -d @"${epoch:=0}" "$file"
    fi
  done
fi

# remove source dir (clean)
if [ -n "$clean" ]; then
  rm -fr "${project:=srcpkg2git}$version"/
  rm -fr appimage/*.AppDir/
  made_source_dir=
fi
